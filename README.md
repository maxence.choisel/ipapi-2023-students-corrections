# IPAPI 2023 Students Corrections

## Nomenclature de nommage
* exercice`xxx`.py pour un exercice (*e.g.* exercice2.py)
* `prenom_nom_proposition`.py pour un problème (*e.g.* alexis_lebis_proposition.py). **Le fichier python doit se trouver dans le repertoire du problème**.

Pour un problème, puisque vous êtes libre dans sa modélisation informatique, il est important qu'il soit lié à un étudiant. De cette manière, plusieurs propositions étudiantes peuvent être réalisées. Toutes les situations d'un même problème se trouve dans le fichier python.

## Cas des problèmes initiaux
Vous pouvez également implémenter les problèmes que nous avons vu précédement en cours. N'hésitez pas à créer de nouveau dossier (respectant la charte de nommage).

## GitLab
Vous pouvez créer de nouveau fichier directement dans l'interface web de GitLab.
Toutefois, renseignez vous sur les gestionnaires de versions, ici GitLab, et comment vous en servir. Notamment les concepts de Pull, Push, Commit, etc...

⚠️ Ces connaissances ne seront jamais demandées en examen. Néanmoins, travailler ces compétences vous donnera un avantage significatif sur vos postes plus tard.
